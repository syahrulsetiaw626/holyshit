<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            ['name' => 'Read User'],
            ['name' => 'Read Employee'],
            ['name' => 'Read Role'],
            ['name' => 'Read Permission'],
            ['name' => 'Create User'],
            ['name' => 'Create Employee'],
            ['name' => 'Create Role'],
            ['name' => 'Create Permission'],
            ['name' => 'Update User'],
            ['name' => 'Update Employee'],
            ['name' => 'Update Role'],
            ['name' => 'Update Permission'],
            ['name' => 'Delete User'],
            ['name' => 'Delete Employee'],
            ['name' => 'Delete Role'],
            ['name' => 'Delete Permission'],
        ])->each(function($data){
            Permission::create($data);
        });

        $permissions = Permission::get();
        $roles = Role::get();

        foreach ($roles as $key => $role) {
            foreach ($permissions as $key => $permission) {
                $role->givePermissionTo($permission);
            }
        }
    }
}
