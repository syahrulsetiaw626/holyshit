<?php

use App\Http\Controllers\AjaxTableController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\PermissionAPIController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RolesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function () {

    Route::post('login', [AuthController::class, "login"]);
    Route::post('logout', [AuthController::class, "logout"]);
    Route::post('refresh', [AuthController::class, "refresh"]);
    Route::post('me', [AuthController::class, "me"]);
});

Route::resource('permission', PermissionAPIController::class);
Route::resource('roles', RolesController::class);

Route::post('ajax-data-table', [AjaxTableController::class, 'ajaxDataTable'])->name('ajax-data-table');