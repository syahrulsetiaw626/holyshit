<?php

use App\Http\Controllers\PermissionController;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});
Route::get('/dashboard', function () {
    return view('admin.dashboard');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function()
{
    //All the routes that belongs to the group goes here
    Route::get('permission', [PermissionController::class, 'index'])->name('permission.index');
});
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
