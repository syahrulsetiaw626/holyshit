<div class="sticky">
    <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
    <div class="app-sidebar">
        <div class="side-header">
            <a class="header-brand1" href="{{ url('/') }}">
                <img src="../assets/images/brand/logo.png" class="header-brand-img desktop-logo" alt="logo">
                <img src="../assets/images/brand/logo-1.png" class="header-brand-img toggle-logo" alt="logo">
                <img src="../assets/images/brand/logo-2.png" class="header-brand-img light-logo" alt="logo">
                <img src="../assets/images/brand/logo-3.png" class="header-brand-img light-logo1" alt="logo">
            </a>
            <!-- LOGO -->
        </div>
        <div class="main-sidemenu">
            <div class="slide-left disabled" id="slide-left"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191"
                    width="24" height="24" viewBox="0 0 24 24">
                    <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z" /></svg>
            </div>
            <ul class="side-menu">
                <li class="sub-category">
                    <h3>Main</h3>
                </li>
                <x-basic-sidebar-component link="{{ url('/') }}" activate="">
                    <x-slot name="icon"><i class="side-menu__icon fe fe-home"></i></x-slot>
                    <x-slot name="text">Dashboard</x-slot>
                </x-basic-sidebar-component>
                <x-basic-sidebar-component link="{{ url('/') }}" activate="">
                    <x-slot name="icon"><i class="side-menu__icon fe fe-home"></i></x-slot>
                    <x-slot name="text">User Management</x-slot>
                </x-basic-sidebar-component>
                <x-basic-sidebar-component link="{{ url('/') }}" activate="">
                    <x-slot name="icon"><i class="side-menu__icon fe fe-user-check"></i></x-slot>
                    <x-slot name="text">Roles</x-slot>
                </x-basic-sidebar-component>
                <x-basic-sidebar-component link="{{ route('permission.index') }}" activate="">
                    <x-slot name="icon"><i class="side-menu__icon fe fe-unlock"></i></x-slot>
                    <x-slot name="text">Permission</x-slot>
                </x-basic-sidebar-component>
                
                

                {{-- contoh --}}
                <li class="sub-category">
                    <h3>Sub Category Menu</h3>
                </li>
                <x-basic-sidebar-component link="{{ url('/') }}" activate="">
                    <x-slot name="icon"><i class="side-menu__icon fe fe-home"></i></x-slot>
                    <x-slot name="text">Basic SideBar</x-slot>
                </x-basic-sidebar-component>
                
                <x-multi-sidebar-component title="Multi Sidebar">
                    <x-slot name="icon"><i class="side-menu__icon fe fe-map-pin"></i></x-slot>
                    <x-sub-multi-sidebar-component link="dadasd" title="Submenu"></x-sub-multi-sidebar-component>
                    <x-sub-multi-sidebar-component link="dasdhjasd" title="Submenu"></x-sub-multi-sidebar-component>
                    <x-sub-multi-sidebar-component link="dadasd" title="Submenu"></x-sub-multi-sidebar-component>
                </x-multi-sidebar-component>
                {{-- contoh --}}
            </ul>
            <div class="slide-right" id="slide-right"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24"
                    height="24" viewBox="0 0 24 24">
                    <path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z" />
                </svg></div>
        </div>
    </div>
    <!--/APP-SIDEBAR-->
</div>
