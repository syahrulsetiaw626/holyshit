<ul class="nav nav-primary">
    
    <x-single-list-sidebar-component link="" activate="">
        <x-slot name="icon"><i class="la flaticon-layers-1"></i></x-slot>
        <x-slot name="text">Dashboard</x-slot>
    </x-single-list-sidebar-component>

    {{-- ini contoh sidebar dropdown --}}
    <x-multiple-list-sidebar-component idSidebar="chart">
        <x-slot name="icon"><i class="la flaticon-interface-1"></i></x-slot>
        <x-slot name="text">User</x-slot>
        {{-- ini contoh untuk sub sidebarnya, link dan activate silahkan ganti sesuai penggunaan --}}
        <x-sublist-multiple-sidebar-component link="/index.html" activate="sublist">
            <x-slot name="text">list</x-slot>
        </x-sublist-multiple-sidebar-component>
    </x-multiple-list-sidebar-component>
    
    {{-- ini contoh single sidebar --}}
    <x-single-list-sidebar-component link="/index.html" activate="index">
        <x-slot name="icon"><i class="la flaticon-interface-1"></i></x-slot>
        <x-slot name="text">User</x-slot>
        <x-slot name="notif">Hallo</x-slot>
    </x-single-list-sidebar-component>
    
    
</ul>
