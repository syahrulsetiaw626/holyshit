<li class="nav-item">
        <a data-toggle="collapse" href="{{ "#".$idSidebar }}">
            {{-- <i class="far fa-chart-bar"></i> --}}
            {{ $icon }}
            <p>{{ $text }}</p>
            <span class="caret"></span>
        </a>
        <div class="collapse" id="{{ $idSidebar }}">
            <ul class="nav nav-collapse">
                {{ $slot }}
                {{-- <x-sublist-multiple-sidebar-component></x-sublist-multiple-sidebar-component>
                <x-sublist-multiple-sidebar-component></x-sublist-multiple-sidebar-component> --}}
            </ul>
        </div>
    </li>