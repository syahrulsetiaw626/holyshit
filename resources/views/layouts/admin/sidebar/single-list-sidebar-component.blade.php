<li class="nav-item @if (Request::segment(1) == $activate) 
        active 
    @endif">
    <a href="{{ $link }}">
        {{ $icon }}
        <p>{{ $text }}</p>
        @isset($notif)
        <span class="badge badge-count">{{ $notif }}</span>
        @endisset
    </a>
</li>

