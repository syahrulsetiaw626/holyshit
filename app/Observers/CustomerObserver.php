<?php

namespace App\Observers;

use App\Models\Customer;
use App\Services\CodeService;

class CustomerObserver
{
    public function creating(Customer $Customer){
        $code = new CodeService();
        $Customer->Customer_code = $code->generateCode();
    }
}
