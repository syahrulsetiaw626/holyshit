<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MultipleListSidebarComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $idSidebar;

    public function __construct($idSidebar)
    {
        $this->idSidebar = $idSidebar;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('layouts.admin.sidebar.multiple-list-sidebar-component');
    }
}
