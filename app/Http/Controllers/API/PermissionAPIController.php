<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(){
        try {
            $data['permissions'] = Permission::latest()->get();
            $data['headTable'] = ['name', 'guard_name'];
            $data['body'] = Permission::latest()->get();
            return response()->json([
                'message' => 'Success get data',
                'data' => $data
            ], 200);
            
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed get data',
                'error' => $e,
                'errorMessage' => $e->errorInfo[2]
            ], 500);
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $name = $request->name;
            $permission = Permission::create(['name' => $name]);

            return response()->json([
                'message' => 'Success storing data',
                'data' => $permission
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'message' => 'Failed storing data',
                'error' => $e,
                'errorMessage' => $e->errorInfo[2]
            ], 500);
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $permission = Permission::find($id);
            return response()->json([
                'message' => 'Success update data',
                'data' => $permission
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'message' => 'Failed update data',
                'error' => $e,
                'errorMessage' => $e->errorInfo[2]
            ], 500);
            
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $name = $request->name;
            $permission = Permission::find($id)->update(['name' => $name]);
            $permission = Permission::find($id);
            return response()->json([
                'message' => 'Success update data',
                'data' => $permission
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'message' => 'Failed update data',
                'error' => $e,
                'errorMessage' => $e->errorInfo[2]
            ], 500);
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $permission = Permission::find($id)->delete();
            return response()->json([
                'message' => 'Success delete data',
            ], 200);
        } catch (QueryException $e) {

            return response()->json([
                'message' => 'Failed delete data',
                'error' => $e,
                'errorMessage' => $e->errorInfo[2]
            ], 500);
            
        }
    }
}
