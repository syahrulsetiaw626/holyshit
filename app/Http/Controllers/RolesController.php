<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data['roles'] = Role::latest()->get();
            // return response()->json([
            //     'message' => 'Success get data roles',
            //     'data' => $data
            // ], 200);
            return view('admin.roles.index', $data);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed get data roles',
                'error' => $e,
                'error_message' => $e->errorInfo[2],
            ], 500);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data['role'] =  Role::create(['name' => $request->name]);
            return response()->json([
                'message' => 'Success store data roles',
                'data' => $data
            ], 200);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed store data roles',
                'error' => $e,
                'error_message' => $e->errorInfo[2],
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $data['roles'] = Role::find($id);
            $data['roles']['permissions'] = $data['roles']->getPermissionNames();
            return response()->json([
                'message' => 'Success get data roles',
                'data' => $data
            ], 200);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed get data roles',
                'error' => $e,
                'error_message' => $e->errorInfo[2],
            ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data['role'] =  Role::find($id)->update(['name' => $request->name]);
            $data['role'] =  Role::find($id);
            return response()->json([
                'message' => 'Success update data roles',
                'data' => $data
            ], 200);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed update data roles',
                'error' => $e,
                'error_message' => $e->errorInfo[2],
            ], 500);
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $data['role'] =  Role::find($id)->delete();
            return response()->json([
                'message' => 'Success delete data roles',
                'data' => $data
            ], 200);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed delete data roles',
                'error' => $e,
                'error_message' => $e->errorInfo[2],
            ], 500);
        }
    }
}
