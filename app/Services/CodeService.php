<?php

namespace App\Services;

use Carbon\Carbon;

/**
 * Class CodeService
 * @package App\Services
 */
class CodeService
{
    public function generateCode($prefix = ""){
        $today = Carbon::now()->isoFormat("YMDhms");
        $rand = rand(100,999);
        $code = $today . $rand;
        return $prefix.$code;
    }
    
    public function generateCodeFromName($name, $prefix = ""){
        $today = Carbon::now()->isoFormat("YMDhms");
        $rand = rand(100,999);
        $code = $today . $rand;
        return $prefix.$code;
    }
}
