<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Class ImageStorageService
 * @package App\Services
 */
class ImageStorageService
{
    public function uploadImage(Request $request, $module){
        try {
            $date = Carbon::now()->isoFormat("YYYYMMDDhhmmss");
            $image = $request->file('image');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension() . "_" . $date;
            $path =  'image/'.$module.'/' . $imageName;
            $image->storeAs('image/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]
            ];
        } catch (QueryException $e) {
            // return $e;
            return [
                'status' => false,
                'message' => $e
            ];
        }
        
    }

    public function updateImage(Request $request, $module, $fileBefore){
        
        try {
            $date = Carbon::now()->isoFormat("YYYYMMDDhhmmss");
            Storage::disk('public')->delete($fileBefore);
            $image = $request->file('image');
            $imageName = date('YmdHis') . "." . $image->getClientOriginalExtension() . "_" . $date;
            $path =  'image/'.$module.'/' . $imageName;
            $image->storeAs('image/'.$module.'/', $imageName, ['disk' => 'public']);
            $size = $image->getSize();
            $extension = $image->getClientOriginalExtension();
            return [
                'status' => true,
                'message' => $path,
                'data' => [
                    'name' => $imageName,
                    'path' => $path,
                    'size' => $size,
                    'extension' => $extension
                ]

            ];
        } catch (QueryException $e) {
            return [
                'status' => false,
                'message' => $e
            ];
        }
    }
    
    public function deleteImage($fileBefore){
        try {
            Storage::disk('public')->delete($fileBefore);
            return true;
        } catch (QueryException $e) {
            return $e;
        }
    }
}
